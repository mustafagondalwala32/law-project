<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('client/ajax-search',[App\Http\Controllers\ClientController::class, 'searchClient'])->name('client.searchajax');
Route::get('users/ajax-search',[App\Http\Controllers\HomeController::class, 'searchUser'])->name('users.searchajax');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


    Route::get('client/add',[App\Http\Controllers\ClientController::class, 'editClient'])->name('client.add');
    Route::get('client/list',[App\Http\Controllers\ClientController::class, 'listClient'])->name('client.list');
    Route::post('/client/store',[App\Http\Controllers\ClientController::class, 'storeClient'])->name('client.store');
    Route::get('client/{id}',[App\Http\Controllers\ClientController::class, 'editClient'])->name('client.edit');

    
    Route::get('case/add',[App\Http\Controllers\CaseController::class, 'editCase'])->name('case.add');
    Route::get('case/list',[App\Http\Controllers\CaseController::class, 'listCase'])->name('case.list');
    Route::post('/case/store',[App\Http\Controllers\CaseController::class, 'storeCase'])->name('case.store');
    Route::get('case/{id}',[App\Http\Controllers\CaseController::class, 'editCase'])->name('case.edit');
    Route::get('case/delete/{type}/{id}',[App\Http\Controllers\CaseController::class, 'deleteCaseElement'])->name('case.delete');





    Route::get('/users/add',[App\Http\Controllers\HomeController::class, 'editUser'])->name('users.add');
    Route::post('/users/store',[App\Http\Controllers\HomeController::class, 'storeUser'])->name('users.store');
    Route::get('/users/list',[App\Http\Controllers\HomeController::class, 'listUsers'])->name('users.list');
    Route::get('/users/{id}',[App\Http\Controllers\HomeController::class, 'editUser'])->name('users.edit');
});

