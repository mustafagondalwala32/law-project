<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
class ClientController extends Controller
{

    
    /**
     * Show the view for client add or update
     * @param Request $request
     * @param null $id
     * @return [type]
     */
    public function editClient(Request $request,$id = null){
        $data['user'] = null;
        if($id)
            $data['user'] = Client::whereId($id)->first();
        return view('client.edit',$data);
    }
    public function listClient(){
        
        $data['client'] = Client::get();
        return view('client.list',$data);
    }
    
    
    
    /**
     * 
     * Add or Update Client
     * @param Request $request
     * @return [type]
     */
    public function storeClient(Request $request){
        if($request->id == null){
            $request->validate([
                'name'=>'required',
                'father_name'=>'required',
                'dob'=>'required',
                'marital_status'=>'required',
                'present_address'=>'required',
                'mobile_number'=>'required|unique:clients,mobile_number,'.$request->id,
                'email_address'=>'required|unique:clients,email_address,'.$request->id,
                'client_type'=>'required',
                'aadhar_file'=>'required,'.$request->id,
                'status'=>'required'
            ]);
        }
        
        $newClient = Client::updateOrCreate(['id'=>$request->id],[
            'name'=>$request->name,
            'designation'=>$request->designation,
            'name'=>$request->name,
            'father_name'=>$request->father_name,
            'mother_name'=>$request->mother_name,
            'dob'=>$request->dob,
            'marital_status'=>$request->marital_status,
            'doa'=>$request->doa,
            'present_address'=>$request->present_address,
            'permanent_address'=>$request->permanent_address,
            'mobile_number'=>$request->mobile_number,
            'mobile_number_2'=>$request->mobile_number_2,
            'email_address'=>$request->email_address,
            'email_address_2'=>$request->email_address_2,
            'client_type'=>$request->client_type,
            'company_name'=>$request->company_name,
            'registered_office'=>$request->registered_office,
            'corporate_office'=>$request->corporate_office,
            'tel_office'=>$request->tel_office,
            'tel_office_2'=>$request->tel_office_2,
            'website'=>$request->website,
            'company_email_address'=>$request->company_email_address,
            'company_email_address_2'=>$request->company_email_address_2,
            'gst_number'=>$request->gst_number,
            'occupation_org_name'=>$request->occupation_org_name,
            'occupation_designation'=>$request->occupation_designation,
            'occupation_address'=>$request->occupation_address,
            'occupation_email'=>$request->occupation_email,
            'comment'=>$request->comment,
            'status'=>$request->status,

        ]);
        
        $request->pan_file ? Client::updateOrCreate(['id'=>$newClient->id],['pan_file'=> $this->fileUpload($request->pan_file)]) : '';
        $request->aadhar_file ? Client::updateOrCreate(['id'=>$newClient->id],['aadhar_file'=> $this->fileUpload($request->aadhar_file)]) : '';
        $request->passport_number_file ? Client::updateOrCreate(['id'=>$newClient->id],['passport_number_file'=> $this->fileUpload($request->passport_number_file)]) : '';
        $request->passport_photo_file ? Client::updateOrCreate(['id'=>$newClient->id],['passport_photo_file'=> $this->fileUpload($request->passport_photo_file)]) : '';
        $request->company_pan_file ? Client::updateOrCreate(['id'=>$newClient->id],['company_pan_file'=> $this->fileUpload($request->company_pan_file)]) : '';
        $request->company_cin_file ? Client::updateOrCreate(['id'=>$newClient->id],['company_cin_file'=> $this->fileUpload($request->company_cin_file)]) : '';
        $request->gst_file ? Client::updateOrCreate(['id'=>$newClient->id],['gst_file'=> $this->fileUpload($request->gst_file)]) : '';

        return back()->with('success','Client updated !!');
    }

    /**
     * @param Request $request
     * @return [type]
     */
    public function searchClient(Request $request){
        $request->validate([
            '_type'=>'required',
            'q'=>'required',
        ]);
        $q = $request->get('q');

        $client = Client::orWhere('name', 'like', '%' . $q . '%')->
                            orWhere('present_address', 'like', '%' . $q . '%')->
                            orWhere('mobile_number', 'like', '%' . $q . '%')->
                            orWhere('email_address', 'like', '%' . $q . '%')->
                            orWhere('company_name', 'like', '%' . $q . '%')->get();
        $sendData = [];
        foreach($client as $c){
            $sendData[] = [
                'id'=>$c->id,
                'text'=>$c->name.' - '.$c->mobile_number.' - '.$c->email_address.' - '.$c->id
            ];
        }
        echo json_encode([
                "results"=>$sendData
        ]);
    }
}

