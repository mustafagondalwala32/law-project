<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CaseCategory;
use App\Models\Cases;
use App\Models\CaseHandleBy;
use App\Models\CaseClient;
use App\Models\CaseDocument;
use App\Models\CaseHistory;
class CaseController extends Controller
{
    /**
     * @param Request $request
     * 
     * @return [type]
     */
    public function editCase(Request $request,$id=null){
        $case = null;
        if($id != null){
            $case = Cases::with(['case_client','case_handle_by','case_documents','type','history'])->find($id);
        }
        $case_category = CaseCategory::where('status',1)->get();
        return view('case.edit',compact('case','case_category'));
    }
    public function listCase(Request $request){
        $cases = Cases::with(['type','case_client','case_handle_by'])->get();
        return view('case.list',compact('cases'));
    }
    public function storeCase(Request $request){
        if($request->id == null){
            $request->validate([
                'case_no'=>'required',
                'client'=>'required',
                'date'=>'required',
                'remark'=>'required',
                'handleBy'=>'required',
                'status'=>'required',
            ]);
        }
        $user = $request->user();

        $newCase = Cases::updateOrCreate(['id'=>$request->id],[
            'case_no'=>$request->case_no,
            'date'=>$request->date,
            'remark'=>$request->remark,
            'status'=>$request->status,
            'case_type'=>$request->case_type,
        ]);
        if($request->handleBy != null){

            foreach($request->handleBy as $handleId){
                $total = CaseHandleBy::where('case_id',$newCase->id)->where('user_id',$handleId)->count();
                if($total == 0){
                    CaseHandleBy::create([
                        'case_id'=>$newCase->id,
                        'user_id'=>$handleId
                    ]);
                }
            }
        }
        if($request->client != null){
            foreach($request->client as $clientId){
                $total = CaseClient::where('case_id',$newCase->id)->where('client_id',$clientId)->count();
                if($total == 0){
                    CaseClient::create([
                        'case_id'=>$newCase->id,
                        'client_id'=>$clientId
                    ]);
                }
            }
        }
        
        if($request->documents != null){
            foreach($request->documents as $document){
                CaseDocument::create([
                    'case_id'=>$newCase->id,
                    'file_name'=>$document->getClientOriginalName(),
                    'path'=>$this->fileUpload($document),
                    'user_id'=>$user->id
                ]);
            }
        }
        
        CaseHistory::create([
            'case_id'=>$newCase->id,
            'user_id'=>$user->id
        ]);

        return back()->with('success','Case updated !!');
    }
    public function deleteCaseElement(Request $request,$type,$id){
        $message = "";
        switch($type){
            case "client":
                CaseClient::find($id)->delete();
                $message = "Client Deleted Successfully";
                break;
            case "handle_by":
                CaseHandleBy::find($id)->delete();
                $message = "User Deleted Successfully";
                break;
            case "document":
                CaseDocument::find($id)->delete();
                $message = "Document Deleted Successfully";
                break;
        }
        return back()->with('success',$message);
    }
}
