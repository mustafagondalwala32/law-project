<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cases extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function case_client(){
        return $this->hasMany(CaseClient::class,'case_id','id')->with('client');
    }
    public function case_handle_by(){
        return $this->hasMany(CaseHandleBy::class,'case_id','id')->with('user');
    }
    public function case_documents(){
        return $this->hasMany(CaseDocument::class,'case_id','id')->with('user');
    }
    public function history(){
        return $this->hasMany(CaseHistory::class,'case_id','id')->with('user');
    }
    public function type(){
        return $this->hasOne(CaseCategory::class,'id','case_type');
    }
}
