<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('designation')->nullable();
            $table->string('father_name');
            $table->string('mother_name')->nullable();
            $table->date('dob');
            $table->boolean('marital_status')->default(0);
            $table->date('doA')->nullable();
            $table->text('present_address');
            $table->text('permanent_address')->nullable();
            $table->string('mobile_number')->unique();
            $table->string('mobile_number_2')->nullable();
            $table->string('email_address')->unique();
            $table->string('email_address_2')->nullable();
            $table->string('pan_file')->nullable();
            $table->string('aadhar_file')->nullable();
            $table->string('passport_number_file')->nullable();
            $table->string('passport_photo_file')->nullable();

            $table->enum('client_type',[1,2]);

            $table->string('company_type')->nullable();
            $table->string('company_name')->nullable();
            $table->string('registered_office')->nullable();
            $table->string('corporate_office')->nullable();
            $table->string('tel_office')->nullable();
            $table->string('tel_office_2')->nullable();
            $table->string('website')->nullable();
            $table->string('company_email_address')->nullable();
            $table->string('company_email_address_2')->nullable();
            $table->string('company_pan_file')->nullable();
            $table->string('company_cin_file')->nullable();
            $table->string('gst_number')->nullable();
            $table->string('gst_file')->nullable();

            $table->tinyInteger('occuption_type')->nullable();
            $table->string('occupation_org_name')->nullable();
            $table->string('occupation_designation')->nullable();
            $table->string('occupation_address')->nullable();
            $table->string('occupation_email')->nullable();


            $table->text('comment');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
